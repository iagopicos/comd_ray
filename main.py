import time
import ray
from ray.util.queue import Queue, Empty
from tornado.ioloop import IOLoop
import matplotlib.pyplot as plt
from CoMD.Analysis.analysis import Analysis
from CoMD.Graphics.graphics import PlotActor
from CoMD.Supervisor.supervisor import Supervisor
from command.cmd_parser import NUM_PORTS, N_RECEIVERS, N_ANALYZERS, PORT, HOST, TIMEOUT, TASK_ACTOR
from utils import loggin_manager
import numpy as np

if __name__ == '__main__':
    ray.init(ignore_reinit_error=True, local_mode=False, include_dashboard=True)

    log = loggin_manager.get_logger()
    queue = Queue()
    loop = IOLoop.current()

    log.info(f'\n ###### COMMANDS ######  \n PORTS: {NUM_PORTS} TASKS:{N_RECEIVERS} TIMEOUT:{TIMEOUT}')
    log.info(f'RECEIVERs: {N_RECEIVERS}')

    socket_list = Supervisor.config(NUM_PORTS, PORT, HOST, N_RECEIVERS)
    actor_supervisor = Supervisor.remote(socket_list, 30000, queue, N_ANALYZERS, TASK_ACTOR)
    actor_plot = PlotActor.remote()

    receiver_list = ray.get(actor_supervisor.start_receiver.remote())

    data_tmp = dict()
    # breakpoint()
    while True:
        processor_list = ray.get(actor_supervisor.start_processor.remote())
        for atom_count in processor_list:
            actor_plot.update_plot.remote(atom_count)
            actor_plot.show_plot.remote()
