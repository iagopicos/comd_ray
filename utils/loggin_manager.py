import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - [%(levelname)s] -- %(message)s')
log = logging.getLogger('ray.serve')


def get_logger():
    return log
