# COMD_RAY
The project uses the Ray python library to retrieve data from ZMQ and analyze this data.
The way its works would be you define how many ray task are going to retrieve data and how many data are going to perform the analysis.
Below Are list the modules with a brief description of his functions


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

##  Command line arguments
- "--init_port, -i" First por to be used
- "--num_ports, -np" Number of ports to listen, Will incremeant since the init port
- "--num_task, -n"  Number of task in parallel to be executed
- "--Host, -ht" Host that will be listening for the zmq sockets
## Modules 
- Supervisor, supervisor.py
  - Class where the workers are going to be cnofigurated.
  - When calling to the method work all all the workers in the list are goint to be launched
- Receiver
  - Class that manage to receive the data
  - When its called it will been executed in an infinte loop waitin for receiving the data in the ports selected
  - When data is received it will put it in the queue for the analyzers to consume
  - TODO. Include some tool to select the analisys type 
- Analyzer
  - Class that manage the analysis of the data
  - It will revieve the data raw an then will parse the data , perform the analysis and also save 
    some information in a txt file  
## Execution
To execute it you should run an instance of the CoMD project:
```
mpirun -np 4 --oversubscribe ../bin/CoMD-mpi --port 9000 --portNum 4 --hostDir ~/tmp/  --nx 40 --ny 40 --nz 40 --hwm 100 --xproc 2 --yproc 1 --zproc 2 
```

Example of python ray parameters for the execution
```
--num_task 2 --init_port 9000 --num_ports 4 --receivers 4 --analyzers 2 --timeout 600000 --host tcp://127.0.0.1

```