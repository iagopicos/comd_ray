import os
import ray
import zmq
from zmq.eventloop.ioloop import IOLoop
from zmq.eventloop import zmqstream
from utils import loggin_manager

log = loggin_manager.get_logger()


@ray.remote
class Receiver:
    def __init__(self, sockets_list=None, timeout=60000, queue=None):
        if sockets_list is None:
            sockets_list = []

        self.sockets_list = sockets_list
        self.context = zmq.Context()
        self.zmq_stream_sockets = []
        self.timeout = timeout
        self.init_sockets_list(self.sockets_list)
        self.queue = queue

    def init_sockets_list(self, init_list) -> None:
        for sckt in init_list:
            new_receiver = self.context.socket(zmq.PULL)
            new_receiver.connect(sckt)
            self.zmq_stream_sockets.append(zmqstream.ZMQStream(new_receiver))

    def add_to_queue(self, data):
        self.queue.put(data)

    def recv(self):
        log.info(f'Entro en el receiver, {os.getpid()}')
        for stream in self.zmq_stream_sockets:
            stream.on_recv(lambda msg: self.queue.put(msg))
        IOLoop.current().start()
