import numpy as np
import ray

from CoMD.Analysis.analysis import Analysis
from CoMD.Reciever.receiver import Receiver
from utils import loggin_manager

log = loggin_manager.get_logger()


@ray.remote
class Supervisor:
    def __init__(self, sockets_list=None, timeout=60000, queue=None, n_processors=1,taks_actor=1):
        if sockets_list is None:
            sockets_list = []
        self.sockets_list = sockets_list
        self.receivers = [Receiver.remote(tmp, timeout, queue) for tmp in sockets_list]
        self.analyzers = [Analysis.remote(queue) for _ in range(n_processors)]
        self.task_actor=taks_actor

    @staticmethod
    def config(n_ports, initial_port, host, n_tasks):
        socket_list = []
        for i in range(n_ports):
            socket = host + ':' + str(int(initial_port) + i)
            socket_list.append(socket)

        log.info(f'Ports Initialized:[ {socket_list} ]')
        return np.array_split(socket_list, n_tasks)

    def start_receiver(self):
        return [w.recv.remote() for w in self.receivers]

    def start_processor(self):
        tasks_id = []
        for actor in self.analyzers:
            for _ in range(self.task_actor):
                tasks_id.append(actor.process_msg.remote())
        return ray.get(tasks_id)

    def terminate(self):
        [ray.kill(w) for w in self.workers]


def init_number_of_sockets(n_ports, initial, host, n_tasks):
    result_list = []
    for i in range(n_ports):
        socket = host + ':' + str(int(initial) + i)
        result_list.append(socket)
    log.info(f'Ports Initialized:[ {result_list} ]')
    return np.array_split(result_list, n_tasks)
