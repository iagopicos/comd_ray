# ACTOR IN CHARGE OF  PLOTTING THE DATA
import time
import matplotlib
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt

# plt.rcParams['animation.html'] = 'jshtml'
import numpy as np
import ray
from utils import loggin_manager

log = loggin_manager.get_logger()
plt.ion()


@ray.remote
class PlotActor:
    def __init__(self):
        self.fig, self.ax = plt.subplots()

    def update_plot(self, positions):
        # breakpoint()
        x_vals = list(positions.keys())
        y_vals = positions.values()
        self.ax.clear()
        # breakpoint()
        self.ax.bar(x_vals, y_vals)
        self.ax.set_xlabel('Z-Order Value')
        self.ax.set_ylabel('Atom Count')
        # log.info(f'PLot updated {y_vals}')
        plt.pause(0.05)

    def show_plot(self):
        log.info('Showing plot ......')
        self.fig.canvas.draw_idle()
