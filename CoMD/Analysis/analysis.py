import os
import struct

import numpy as np
import ray
from ray.util.queue import Empty
from datetime import datetime

from utils import loggin_manager

log = loggin_manager.get_logger()


@ray.remote
class Analysis:
    def __init__(self, queue=None):
        self.atoms_list = {}
        self.queue = queue
        self.buffer_data = {}
        self.date = datetime.today().strftime('%Y-%m-%d--%H:%M')
        self.atom_counter = {}

    def process_data(self, data):
        log.info(f' Analyzer {os.getpid()} start processing the data')
        # TODO improve this loop
        i = 0
        for atom_id in data[3]:
            self.atoms_list[atom_id] = (data[4][i:i + 3])
            i += 3

        f = open(f'output/Ray_{str(data[0])}_{self.date}.txt', 'a')
        for atom in self.atoms_list:
            # breakpoint()
            f.write(f'Atom id: {str(atom)}')
            f.write('\n')
            f.write(str(self.atoms_list[atom]))
            f.write('\n')
        f.close()

    @staticmethod
    def process_raw_msg(raw_msg):
        return_data = []
        size = len(raw_msg[0]) // 4
        # Rank
        return_data.append(struct.unpack('i' * size, raw_msg[0])[0])
        size = len(raw_msg[1]) // 4
        # ts
        return_data.append(struct.unpack('i' * size, raw_msg[1]))
        # timestamp
        return_data.append(struct.unpack('l', raw_msg[2]))
        size = len(raw_msg[3]) // 4
        # atom idBuffer
        return_data.append(struct.unpack('i' * size, raw_msg[3]))
        size = len(raw_msg[4]) // 8
        # buffer atom positions
        return_data.append(struct.unpack('d' * size, raw_msg[4]))

        return return_data

    def __add_to_buffer(self, msg): #TODO review
        rank = msg[0]
        if rank in self.buffer_data.keys():
            self.buffer_data[rank].append(msg)
        else:
            self.buffer_data[rank] = [msg]

    def process_msg(self):
        log.info(f'Receiver PID: {os.getpid()} cola: {len(self.queue)}')
        try:
            raw_data = self.queue.get(block=True)
            data = self.process_raw_msg(raw_data)
            self.process_data(data)
            self.__add_to_buffer(data)
            self.data_manage(data)
            return self.get_atom_counts()
        except Empty:
            return None

    @staticmethod
    def init_data(self):
        for rank in self.data:
            for msg in self.data[rank]:
                i = 3
                atom_id = msg[3]
                atom_positions = msg[i - 3:i]
                timestamp = msg[2]
                i += 3
                self.processed_data.append(atom_id, atom_positions, timestamp)

    @staticmethod
    def double_to_bits(num):
        bits = struct.unpack('!Q', struct.pack('!d', num))[0]
        return format(bits, '064b')

    def data_manage(self, data):
        i = 0
        for atom_id in data[3]:
            self.atoms_list[atom_id] = (data[4][i:i + 3])
            # z_value = self.z_order_update(*data[4][i:i + 3])
            z_value = self.dummy_task(*data[4][i:i + 3], atom_id)
            if z_value in self.atom_counter.keys():
                # log.info('------ SAME POSITION ------')
                self.atom_counter[z_value] = self.atom_counter[z_value] + 1
                # log.info(f'z: {z_value} count: {self.atom_counter[z_value]}')
            else:
                self.atom_counter[z_value] = 1
            i += 3

    def z_order_update(self, x, y, z):
        x_bits = np.array(list(self.double_to_bits(x)))
        y_bits = np.array(list(self.double_to_bits(y)))
        z_bits = np.array(list(self.double_to_bits(z)))

        z_order_bits = np.column_stack((x_bits, y_bits, z_bits))
        z_order_bits = z_order_bits.flatten()

        z_order_value = np.packbits(z_order_bits.astype(np.uint8))
        z_order_value = z_order_value.view(np.uint64)[0]

        return z_order_value

    def get_atom_counts(self):
        return self.atom_counter

    def dummy_task(self, x, y, z, atom_id):
        return int(z)
