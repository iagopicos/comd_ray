import argparse

# Initialize parser
parser = argparse.ArgumentParser(description='Program options')
# Define arguments
parser.add_argument('--init_port', '-i', metavar='PORT', type=str)
parser.add_argument('--num_ports', '-np', metavar='NUM_PORTS', type=int, default=0)
parser.add_argument('--receivers', '-r', metavar='NUM_TASKS', type=int, default=1)
parser.add_argument('--analyzers', '-a', metavar='NUM_TASKS', type=int, default=1)
parser.add_argument('--timeout', '-t', metavar='TIMEOUT', type=int, default=100000, required=False)
parser.add_argument('--host', '-ht', metavar='HOST', type=str, default='localhost')
parser.add_argument('--task_per_processor', '-ta', metavar='TASK_ACTOR', type=int, default=1)

# Pass arguments to variables
args = parser.parse_args()
PORT = args.init_port
NUM_PORTS = args.num_ports
N_RECEIVERS = args.receivers
N_ANALYZERS = args.analyzers
TIMEOUT = args.timeout
HOST = args.host
TASK_ACTOR = args.task_per_processor


'''
PARAMETERS LIST
[INIT_PORT]  - First por to use
[NUM_PORTS]  - Number of ports that will be listening
[NUM_TASK]   - Number of task that will be recieveing the data
[TIMEOUT]    - Max time the process will be listening for messages
[HOST]       - The host were the sockest will be created
'''
